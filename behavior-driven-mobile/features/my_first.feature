Feature: Podcast feature

  Scenario: As a valid user I add a podcast searching iTunes
    Given I swipe left
    And I touch the "Add Podcast" text
		And I wait
		And I press the "SEARCH ITUNES" button
		And I touch the "Serial" text
		When I press the "SUBSCRIBE" button
		And I wait
		And I press the "OPEN PODCAST" button


