describe('Los estudiantes login', function() {
    beforeEach(function (){
        cy.visit('https://losestudiantes.co');

        cy.contains('Cerrar').click();
    })

    it('Visits los estudiantes and login', function() {
      cy.contains('Ingresar').click()

      cy.get('.cajaLogIn').find('input[name="correo"]').click().type("fernandorb_8@hotmail.com")
      cy.get('.cajaLogIn').find('input[name="password"]').click().type("123456789abcd")
      cy.get('.cajaLogIn').contains('Ingresar').click()

      cy.get('#cuenta').should('exist');
    })

    it('Visits los estudiantes creates account', function() {
      cy.contains('Ingresar').click()

      cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Fernando")
      cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Reyes Bejarano")
      cy.get('.cajaSignUp').find('input[name="correo"]').click().type("fernandorb_8_test_p1@hotmail.com")
      cy.get('.cajaSignUp').find('select[name="idPrograma"]').select("Administración")
      cy.get('.cajaSignUp').find('input[name="password"]').type("123456789abcd")
      cy.get('.cajaSignUp').find('input[name="acepta"]').check()
      cy.contains('Registrarse').click()

      cy.get('#cuenta').should('exist');
    })

    it('Visits los estudiantes and fails at login', function() {
      cy.contains('Ingresar').click()

      cy.get('.cajaLogIn').find('input[name="correo"]').click().type("wrongemail@example.com")
      cy.get('.cajaLogIn').find('input[name="password"]').click().type("1234")
      cy.get('.cajaLogIn').contains('Ingresar').click()

      cy.contains('El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.')
    })
})
