Feature: Register into losestudiantes
  As a user I want to register myself within losestudiantes website in order to rate teachers

Scenario: Register with new user
  Given I go to losestudiantes home screen
  When I open the login screen
  And I write my name Fernando and last names Reyes Bejarano
  And I register with new email fernandorb_8_test3@hotmail.com
  And I select my program Arquitectura
  And I register my new password 123456789abcd
  And I accept the conditions
  And I try to register
  Then I expect a success alert

Scenario: Register with existing user
  Given I go to losestudiantes home screen
  When I open the login screen
  And I write my name Fernando and last names Reyes Bejarano
  And I register with new email fernandorb_8@hotmail.com
  And I select my program Arquitectura
  And I register my new password 123456789abcd
  And I accept the conditions
  And I try to register
  Then I expect a failure alert
