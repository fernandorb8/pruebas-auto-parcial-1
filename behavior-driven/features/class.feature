Feature: See class information
  As a user I expecto to see a class' information

Scenario: Go to class' page
  Given I go to losestudiantes home screen
  And I search "Pruebas automáticas"
  When I click "Pruebas Automáticas - MISO4208 - Ingeniería de Sistemas" search result
  Then I expect to arrive at "MISO4208"'s page
