describe('Los estudiantes class', function() {
    beforeEach(function (){
        cy.visit('https://losestudiantes.co');

        cy.contains('Cerrar').click();

        cy.contains('Ingresar').click()

        cy.get('.cajaLogIn').find('input[name="correo"]').click().type("fernandorb_8@hotmail.com")
        cy.get('.cajaLogIn').find('input[name="password"]').click().type("123456789abcd")
        cy.get('.cajaLogIn').contains('Ingresar').click()
    })

    it('Login to los estudiantes and search class', function() {
      cy.get('.Select-control').click()
      cy.focused().type("Pruebas auto", { force: true });
      cy.contains('Pruebas Automáticas').click()
    })
})
