describe('Los estudiantes teacher', function() {
    beforeEach(function (){
        cy.visit('https://losestudiantes.co');

        cy.contains('Cerrar').click();

        cy.contains('Ingresar').click()

        cy.get('.cajaLogIn').find('input[name="correo"]').click().type("fernandorb_8@hotmail.com")
        cy.get('.cajaLogIn').find('input[name="password"]').click().type("123456789abcd")
        cy.get('.cajaLogIn').contains('Ingresar').click()
    })

    it('Login to los estudiantes and search teacher', function() {
      cy.get('.Select-control').click()
      cy.focused().type("Mario Linares Vasquez", { force: true });
      cy.contains('Mario Linares Vasquez - Ingeniería de Sistemas').click()
    })

    it('Login to los estudiantes and search class of teacher', function() {
      cy.get('.Select-control').click()
      cy.focused().type("Mario Linares Vasquez", { force: true });
      cy.contains('Mario Linares Vasquez - Ingeniería de Sistemas').click()

      cy.get('input[name="id:MISO4208"]').check()
    })
})
