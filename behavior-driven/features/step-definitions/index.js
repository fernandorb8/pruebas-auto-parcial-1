//Complete siguiendo las instrucciones del taller
var {defineSupportCode} = require('cucumber');
var {expect} = require('chai');

defineSupportCode(({Given, When, Then}) => {
  Given('I go to losestudiantes home screen', () => {
    browser.url('/');
    if(browser.isVisible('button=Cerrar')) {
      browser.click('button=Cerrar');
    }
  });

  Given('I search {string}', (search) => {
    browser.element('.Select-placeholder').click()
    browser.elementActive().keys(search.replace(/['"]+/g, ''))
  });

  When('I open the login screen', () => {
    browser.waitForVisible('button=Ingresar', 5000);
    browser.click('button=Ingresar');
  });

  When('I fill a wrong email and password', () => {
    var cajaLogIn = browser.element('.cajaLogIn');

    var mailInput = cajaLogIn.element('input[name="correo"]');
    mailInput.click();
    mailInput.keys('wrongemail@example.com');

    var passwordInput = cajaLogIn.element('input[name="password"]');
    passwordInput.click();
    passwordInput.keys('123467891')
  });

  When('I try to login', () => {
    var cajaLogIn = browser.element('.cajaLogIn');
    cajaLogIn.element('button=Ingresar').click()
  });

  When(/^I fill with (.*) and (.*)$/ , (email, password) => {
     var cajaLogIn = browser.element('.cajaLogIn');

      var mailInput = cajaLogIn.element('input[name="correo"]');
      mailInput.click();
      mailInput.keys(email);

      var passwordInput = cajaLogIn.element('input[name="password"]');
      passwordInput.click();
      passwordInput.keys(password)
  });

  When(/^I write my name (.*) and last names (.*) (.*)$/, (name, firstLastName,
  secondLastName ) => {

    lastNames = firstLastName + " " + secondLastName;

    var cajaSignUp = browser.element('.cajaSignUp');

    var nameInput = cajaSignUp.element('input[name="nombre"]')
    nameInput.click();
    nameInput.keys(name)

    var lastNameInput = cajaSignUp.element('input[name="apellido"]')
    lastNameInput.click()
    lastNameInput.keys(lastNames)

  });

  When(/^I register with new email (.*)$/, (email) => {

    var cajaSignUp = browser.element('.cajaSignUp');

    var mailInput = cajaSignUp.element('input[name="correo"]');
    mailInput.click();
    mailInput.keys(email);

  })

  When(/^I select my program (.*)$/, (program) => {

    browser.selectByVisibleText('select[name="idPrograma"]',program)

  })

  When(/^I register my new password (.*)$/, (password) => {

    var cajaSignUp = browser.element('.cajaSignUp');

    var passwordInput = cajaSignUp.element('input[name="password"]');
    passwordInput.click();
    passwordInput.keys(password);

  })

  When(/^I accept the conditions$/, () => {

    var cajaSignUp = browser.element('.cajaSignUp');

    cajaSignUp.element('input[name="acepta"]').click()

  })

  When (/^I try to register$/, () => {

    var cajaSignUp = browser.element('.cajaSignUp');

    cajaSignUp.element('button=Registrarse').click()

  })

  When("I click {string} search result", profesor => {
    browser.waitForVisible('.Select-option');
    browser.element('div='+profesor.replace(/['"]+/g, '')).click();
  })

  Then('I expect to not be able to login', () => {
    browser.waitForVisible('.aviso.alert.alert-danger', 5000);
  });

  Then('I expect to see {string}', error => {
      browser.waitForVisible('.aviso.alert.alert-danger', 5000);
      var alertText = browser.element('.aviso.alert.alert-danger').getText();
      expect(alertText).to.include(error);
  });

  Then('I expect to have the login icon', () => {
    browser.waitForVisible('#cuenta', 5000)
  });

  Then('I expect a success alert', () => {

    browser.waitForVisible('.sweet-alert > h2');

    expect(browser.element('.sweet-alert > h2').getText()).to.include('Registro exitoso!')

  })

  Then('I expect a failure alert', () => {

    browser.waitForVisible('.sweet-alert > h2');

    expect(browser.element('.sweet-alert > h2').getText()).to.include('Ocurrió un error activando tu cuenta')

  })

  Then("I expect to arrive at {string}'s page", (profesor) => {

    browser.waitForVisible('.nombreProfesor');

    expect(browser.element('.nombreProfesor').getText()).to.include(profesor.replace(/['"]+/g, ''));

  })

});
