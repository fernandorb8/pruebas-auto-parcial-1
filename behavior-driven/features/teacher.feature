Feature: See teacher information
  As a user I expecto to see a teacher's information

Scenario: Go to teacher's page
  Given I go to losestudiantes home screen
  And I search "Mario Linares"
  When I click "Mario Linares Vasquez - Ingeniería de Sistemas" search result
  Then I expect to arrive at "Mario Linares"'s page
